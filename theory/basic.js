///////////////////////
//variable declaration
///////////////////////
var message;
message = 'Hello World';

var name = 'John',
    age = 24;

let count = 10;
if (count > 5) {
    let count = 5; // count = 5
}
// count here still equal 10

const user = {
    name: 'John',
    age: 24
};

const PRIMARY_COLOR = '#ccc';

///////////////////////
// Data Types
///////////////////////
// Boolean
let isValid = true;
isValid = false;
// Number
//integer or floating point number.
const age = 23;
const coef = 12.345;
//The number type has three symbolic values: +Infinity, -Infinity, and NaN (not-a-number).
//String
//A sequence of characters that represent a text value. JavaScript strings are immutable.
let str = "Hello world"; 
str = 'new string';
let age = 23;
let newStr = `age - ${age}`;
console.log(`first line
second line`);
str.length // 10
str.charAt(2) // w
str.toLowerCase() // "new string"
str.toUpperCase() // "NEW STRING"
str.indexOf('str') // 4
newStr.substring(11, 17) // second
newStr.substr(11, 6) // second
//null
//null is a special keyword denoting a null value.
let x = null;
//undefined
//undefined is a top-level property whose value is not defined.
let y; // undefined

///////////////////////
//Type coercion https://www.freecodecamp.org/news/js-type-coercion-explained-27ba3d9a2839/
///////////////////////
//JavaScript variables can be converted to a new variable and another data type either by the use of a JavaScript function or automatically by JavaScript itself:

//Converting to string

String(null) // "null"
false.toString();  // "false"
12 + 'px' // "12px"
//Converting to numbers

Number(true) // 1
parseInt('3.14', 10) // 3
parseFloat('3.14') // 3.14
12 - '4' // 8
12 + '4' // "124"
12 + +'4' // 16

//Converting to boolean

//0, '', null, undefined, NaN -> false
if (12) { }
Boolean(undefined) // false
console.log(!!'text'); // true

///////////////////////
//Control flow https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Control_flow_and_error_handling
///////////////////////

//if...else statement
if (age < 18) {
  ageCategory = 'young';
} else if (age > 50) {
  ageCategory = 'old';
} else {
  ageCategory = 'adult';
}

//switch statement
switch(ageCategory) {
  case 'young': 
    console.log('less than 18');
  case 'adult': 
    console.log('from 18 to 50');
    break;
  case 'old': 
    console.log('older then 50'); 
    break;
  default: 
    console.log('unknown');
}

///////////////////////
//Loops and iteration https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Loops_and_iteration
///////////////////////

//for statement, break statement, continue statement
for (let step = 0; step < stepCount; step++) {
    const value = step * intervalComputed;
  
    if (value === 0) {
        labelsFromRange.push('0');
        continue;
    }
  
    const labelText = formatNumber(value, decimalSeparator);
    const labelWidth = getTextWidth(labelText);
    const isLastStep = step === stepCountWithMargin;
  
    if (isLastStep && remainingAxisLength < labelWidth) {
        break;
    }
  
    labelsFromRange.push(labelText);
}

//while statement
let level = currentLevel;

while(level >= 0) {
  // do something
  level--;
}

//do...while statement
do {
    // do something
    level--;
} while (level >= 0);

//for...of statement
const user = { firstName: 'Mike', lastName: 'Din' };  

for (let value of user) {  
  console.log(value); // Mike, Din  
}

Object.keys(user).forEach((key) => { console.log(key) });

//for...in statement
Array.prototype.customFunction = function() {};

let weekends = ['Sat', 'Sun'];

for (let i in weekends ) {
  console.log(i); // 0, 1, "customFunction"
}
